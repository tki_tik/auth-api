"use strict";

document.addEventListener("DOMContentLoaded", function () {
  window.app = new Vue({
    el: "#app",
    data: function () {
      return {};
    },
    methods: {
      logout: async function () {
        await axios.post("/logout");
        window.location.href = "/login";
      },
    },
  });
});
