"use strict";

document.addEventListener("DOMContentLoaded", function () {
  window.app = new Vue({
    el: "#app",
    data: function () {
      return {
        clientId: new URL(document.location).searchParams.get("clientId"),
        loginBg: "../../img/login_bg_10.jpg",
        isLoading: false,
        isButtonDisabled: true,
        loginId: "",
        password: "",
        code: "",
        authInfo: {},
        hasError: false,
        error: "",
      };
    },
    created: function () {
      const bgNo = Math.floor(Math.random() * 10) + 1;
      this.loginBg = `../../img/login_bg_${bgNo}.jpg`;
    },
    methods: {
      inputForm: function () {
        this.isButtonDisabled = !this.loginId || !this.password;
      },
      onLoginClicked: async function () {
        this.isLoading = true;
        await this.doLogin();
        if (!this.hasError) {
          this.transition();
        }
        this.isLoading = false;
      },
      doLogin: async function () {
        let params = new URLSearchParams();
        params.append("userId", this.loginId);
        params.append("password", this.password);
        try {
          const res = await axios.post("/login", params);
          console.log("code", res.data);
          this.code = res.data;
          this.hasError = false;
          this.error = "";
        } catch (error) {
          this.hasError = true;
          if (error.response) {
            if (error.response.status === 401) {
              this.error = "ログインできません";
            } else {
              this.error = `Login Api Error : ${error.response.status}`;
            }
          } else {
            this.error = `Error : ${error.message}`;
          }
        }
      },
      getAuthInfo: async function () {
        const res = await axios.get(`/api/get-auth-info?code=${this.code}`);
        console.log("userId", res.data.userId);
        console.log("userName", res.data.userName);
        console.log("role", res.data.role);
        this.authInfo = res.data;
      },
      transition: async function () {
        if (this.clientId) {
          window.location.href = `/api/redirect-client-home?clientId=${this.clientId}&code=${this.code}`;
        } else {
          await this.getAuthInfo();
          const target = this.authInfo.role === "ROLE_ADMIN" ? "admin" : "user";
          window.location.href = `/pages/${target}`;
        }
      },
    },
  });
});
