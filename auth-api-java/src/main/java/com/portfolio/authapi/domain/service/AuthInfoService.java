package com.portfolio.authapi.domain.service;

import static com.portfolio.authapi.common.Constant.REDIS_KEY_ROLE;
import static com.portfolio.authapi.common.Constant.REDIS_KEY_USER_ID;
import static com.portfolio.authapi.common.Constant.REDIS_KEY_USER_NAME;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.portfolio.authapi.domain.repository.RedisRepository;
import com.portfolio.authapi.security.dto.AuthInfo;

import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Service
public class AuthInfoService {

    private final RedisRepository repository;

    /**
     * 認証情報取得。
     * 
     * @param code 認可コード
     * @return 認証情報
     * @throws JsonProcessingException
     * @throws JsonMappingException
     */
    public AuthInfo getAuthInfo(String code) throws JsonMappingException, JsonProcessingException {

        // ユーザID取得及び削除
        String userId = this.repository.get(REDIS_KEY_USER_ID + code);
        this.repository.delete(REDIS_KEY_USER_ID + code);

        // ユーザ名取得及び削除
        String userName = this.repository.get(REDIS_KEY_USER_NAME + code);
        this.repository.delete(REDIS_KEY_USER_NAME + code);

        // 権限取得及び削除
        String role = this.repository.get(REDIS_KEY_ROLE + code);
        this.repository.delete(REDIS_KEY_ROLE + code);

        return AuthInfo.builder()
            .userId(userId)
            .userName(userName)
            .role(role)
            .build();
    }

    /**
     * 認証情報設定。
     * 
     * @param authInfo 認証情報
     * @return 認可コード
     * @throws JsonProcessingException
     */
    public String saveAuthInfo(AuthInfo authInfo) throws JsonProcessingException {
        String code = RandomStringUtils.randomAlphanumeric(10);
        this.repository.set(REDIS_KEY_USER_ID + code, authInfo.getUserId());
        this.repository.set(REDIS_KEY_USER_NAME + code, authInfo.getUserName());
        this.repository.set(REDIS_KEY_ROLE + code, authInfo.getRole());
        return code;
    }
}
