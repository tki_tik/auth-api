package com.portfolio.authapi.domain.model;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class UserInfoModel {

    /** ユーザ名 */
    private final String username;

    /** パスワード */
    private final String password;
}
