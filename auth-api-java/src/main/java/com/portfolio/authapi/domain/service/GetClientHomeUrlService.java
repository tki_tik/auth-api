package com.portfolio.authapi.domain.service;

import com.portfolio.authapi.domain.repository.ConnectInfoRepository;

import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Service
public class GetClientHomeUrlService {

    private final ConnectInfoRepository repository;

    /**
     * クライアント先URL取得。
     * 
     * @param clientId クライアントID
     * @return クライアント先URL
     */
    public String getClientHomeUrl(String clientId) {
        return this.repository.searchRedirectUrl(clientId);
    }
}
