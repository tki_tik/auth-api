package com.portfolio.authapi.domain.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.portfolio.authapi.common.Constant.Authority;
import com.portfolio.authapi.domain.model.UserInfoModel;
import com.portfolio.authapi.domain.repository.UserDetailRepository;
import com.portfolio.authapi.domain.repository.UserInfoRepository;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Service
public class SimpleUserDetailsService implements UserDetailsService {

    private final UserInfoRepository userInfoRepository;

    private final UserDetailRepository userDetailRepository;

    /**
     * {@inheritDoc}
     */
    @Override
    public UserDetails loadUserByUsername(String userId) {
        Authority auth = this.userInfoRepository.getAuthority(userId);
        UserInfoModel model = this.userDetailRepository.searchUser(userId);

        // 認証情報設定
        UserDetails userDetails = new User(
            model.getUsername(),
            model.getPassword(),
            this.getAuthorities(auth)
        );

        return userDetails;
    }

    private Collection<? extends GrantedAuthority> getAuthorities(Authority auth) {
        List<GrantedAuthority> authorityList = new ArrayList<>();
        authorityList.add(new SimpleGrantedAuthority(auth.toString()));
        return authorityList;
    }
}