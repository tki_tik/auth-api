package com.portfolio.authapi.domain.repository;

import com.portfolio.authapi.common.Constant.Authority;

public interface UserInfoRepository {

    /**
     * 権限取得。
     * 
     * @param id ユーザID
     * @return 権限
     */
    public Authority getAuthority(String id);
}
