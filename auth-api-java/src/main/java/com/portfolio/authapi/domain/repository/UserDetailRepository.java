package com.portfolio.authapi.domain.repository;

import com.portfolio.authapi.domain.model.UserInfoModel;

public interface UserDetailRepository {

    /**
     * ユーザ検索。
     * 
     * @param userId ユーザID
     * @return ユーザ情報
     */
    public UserInfoModel searchUser(String userId);
}
