package com.portfolio.authapi.domain.repository;

public interface ConnectInfoRepository {

    /**
     * リダイレクトURL検索。
     * 
     * @param clientId クライアントID
     * @return リダイレクトURL
     */
    public String searchRedirectUrl(String clientId);
}
