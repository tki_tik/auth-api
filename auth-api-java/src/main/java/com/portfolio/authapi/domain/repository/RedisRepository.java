package com.portfolio.authapi.domain.repository;

public interface RedisRepository {

    /**
     * KVS情報取得。
     * 
     * @param key キー
     * @return KVS情報
     */
    public String get(String key);

    /**
     * KVS情報設定。
     * 
     * @param key キー
     * @param value KVS情報
     */
    public void set(String key, String value);

    /**
     * KVS情報削除。
     * 
     * @param key キー
     */
    public void delete(String key);
}
