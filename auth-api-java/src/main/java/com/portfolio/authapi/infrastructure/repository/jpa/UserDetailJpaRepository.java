package com.portfolio.authapi.infrastructure.repository.jpa;

import com.portfolio.authapi.infrastructure.entity.UserDetailEntity;

import org.springframework.data.jpa.repository.JpaRepository;

public interface UserDetailJpaRepository extends JpaRepository<UserDetailEntity, String> {
}
