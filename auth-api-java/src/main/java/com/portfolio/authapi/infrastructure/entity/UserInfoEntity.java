package com.portfolio.authapi.infrastructure.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;

import com.portfolio.authapi.common.Constant.Authority;

import lombok.Getter;
import lombok.Setter;

/**
 * ユーザ詳細テーブルエンティティクラス。
 */
@Getter
@Setter
@Entity
@Table(name = "user_info")
public class UserInfoEntity extends AbstractEntity {

    /** id */
    @Id
    @Column(nullable = false, unique = true)
    private String id;

    /** 権限 */
    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private Authority authority;
}
