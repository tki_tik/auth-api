package com.portfolio.authapi.infrastructure.repository.jpa;

import com.portfolio.authapi.infrastructure.entity.UserInfoEntity;

import org.springframework.data.jpa.repository.JpaRepository;

public interface UserInfoJpaRepository extends JpaRepository<UserInfoEntity, String> {
}
