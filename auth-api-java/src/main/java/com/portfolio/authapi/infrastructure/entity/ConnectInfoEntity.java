package com.portfolio.authapi.infrastructure.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

/**
 * 接続情報テーブルエンティティクラス。
 */
@Getter
@Setter
@Entity
@Table(name = "connect_info")
public class ConnectInfoEntity extends AbstractEntity {

    /** クライアントID */
    @Id
    @Column(name = "client_id", nullable = false, unique = true)
    private String clientId;

    /** システム名 */
    @Column(name = "system", nullable = false)
    private String system;

    /** リダイレクトURL */
    @Column(name = "redirect_url", nullable = false)
    private String redirectUrl;
}
