package com.portfolio.authapi.infrastructure.repository.impl;

import com.portfolio.authapi.common.Constant.Authority;
import com.portfolio.authapi.domain.repository.UserInfoRepository;
import com.portfolio.authapi.infrastructure.entity.UserInfoEntity;
import com.portfolio.authapi.infrastructure.repository.jpa.UserInfoJpaRepository;

import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Repository;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Repository
public class UserInfoRepositoryImpl implements UserInfoRepository {

    private final UserInfoJpaRepository jpaRepository;

    /**
     * {@inheritDoc}
     */
    @Override
    public Authority getAuthority(String id) {
        UserInfoEntity entity = this.jpaRepository.findById(id).orElseThrow(
            () -> new UsernameNotFoundException("user not found")
        );
        return entity.getAuthority();
    }
}
