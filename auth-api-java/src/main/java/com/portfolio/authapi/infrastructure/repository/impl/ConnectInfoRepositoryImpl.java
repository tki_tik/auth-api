package com.portfolio.authapi.infrastructure.repository.impl;

import com.portfolio.authapi.domain.repository.ConnectInfoRepository;
import com.portfolio.authapi.infrastructure.entity.ConnectInfoEntity;
import com.portfolio.authapi.infrastructure.repository.jpa.ConnectInfoJpaRepository;

import org.springframework.stereotype.Repository;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Repository
public class ConnectInfoRepositoryImpl implements ConnectInfoRepository {

    private final ConnectInfoJpaRepository jpaRepository;

    /**
     * {@inheritDoc}
     */
    @Override
    public String searchRedirectUrl(String clientId) {
        ConnectInfoEntity entity = this.jpaRepository.findById(clientId).orElseThrow(RuntimeException::new);
        return entity.getRedirectUrl();
    }
}
