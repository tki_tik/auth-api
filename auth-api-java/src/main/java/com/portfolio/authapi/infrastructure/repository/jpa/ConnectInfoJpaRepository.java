package com.portfolio.authapi.infrastructure.repository.jpa;

import com.portfolio.authapi.infrastructure.entity.ConnectInfoEntity;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ConnectInfoJpaRepository extends JpaRepository<ConnectInfoEntity, String> {
}
