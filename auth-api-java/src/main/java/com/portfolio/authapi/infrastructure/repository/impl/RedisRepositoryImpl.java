package com.portfolio.authapi.infrastructure.repository.impl;

import com.portfolio.authapi.domain.repository.RedisRepository;

import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Repository;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Repository
public class RedisRepositoryImpl implements RedisRepository {

    private final StringRedisTemplate redisTemplate;

    /**
     * {@inheritDoc}
     */
    @Override
    public String get(String key) {
        return this.redisTemplate.opsForValue().get(key);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void set(String key, String value) {
        this.redisTemplate.opsForValue().set(key, value);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void delete(String key) {
        this.redisTemplate.delete(key);
    }
}
