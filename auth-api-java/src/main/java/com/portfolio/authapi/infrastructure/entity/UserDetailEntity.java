package com.portfolio.authapi.infrastructure.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

/**
 * ユーザ詳細テーブルエンティティクラス。
 */
@Getter
@Setter
@Entity
@Table(name = "user_detail")
public class UserDetailEntity extends AbstractEntity {

    /** id */
    @Id
    @Column(name = "user_id", nullable = false, unique = true)
    private String userId;

    /** ユーザ名 */
    @Column(name = "user_name", nullable = false)
    private String username;

    /** パスワード */
    @Column(nullable = false)
    private String password;
}
