package com.portfolio.authapi.infrastructure.repository.impl;

import com.portfolio.authapi.domain.model.UserInfoModel;
import com.portfolio.authapi.domain.repository.UserDetailRepository;
import com.portfolio.authapi.infrastructure.entity.UserDetailEntity;
import com.portfolio.authapi.infrastructure.repository.jpa.UserDetailJpaRepository;

import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Repository;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Repository
public class UserDetailRepositoryImpl implements UserDetailRepository {

    private final UserDetailJpaRepository jpaRepository;

    /**
     * {@inheritDoc}
     */
    @Override
    public UserInfoModel searchUser(String userId) {
        UserDetailEntity entity = this.jpaRepository.findById(userId).orElseThrow(
            () -> new UsernameNotFoundException("user not found")
        );
        return UserInfoModel.builder()
            .username(entity.getUsername())
            .password(entity.getPassword())
            .build();
    }
}
