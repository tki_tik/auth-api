package com.portfolio.authapi.security;

import static java.util.stream.Collectors.joining;

import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.portfolio.authapi.domain.service.AuthInfoService;
import com.portfolio.authapi.security.dto.AuthInfo;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * 認証が成功した時の処理
 */
@Slf4j
@RequiredArgsConstructor
@Component
public class SimpleAuthenticationSuccessHandler implements AuthenticationSuccessHandler {

    private final AuthInfoService service;

    @Override
    public void onAuthenticationSuccess(HttpServletRequest req, HttpServletResponse res, Authentication auth)
        throws IOException {
        if (res.isCommitted()) {
            log.info("Response has already been committed.");
            return;
        }

        // HTTPステータス設定
        res.setStatus(HttpStatus.OK.value());

        // 認証情報設定
        String code =
            this.service.saveAuthInfo(this.createAuthInfo(req.getParameterValues("userId")[0], auth));

        // Body設定
        res.setContentType(MediaType.APPLICATION_JSON_VALUE);
        res.setCharacterEncoding(StandardCharsets.UTF_8.name());
        PrintWriter out = res.getWriter();
        out.print(code);
        out.flush();
        out.close();
    }

    private AuthInfo createAuthInfo(String userId, Authentication auth) {
        return AuthInfo.builder()
            .userId(userId)
            .userName(auth.getName())
            .role(auth.getAuthorities().stream().map(GrantedAuthority::getAuthority).collect(joining("\n  ")))
            .build();
    }
}