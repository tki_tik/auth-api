package com.portfolio.authapi.security.dto;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class AuthInfo {

    /** ユーザID */
    private String userId;

    /** ユーザ名 */
    private String userName;

    /** 権限 */
    private String role;
}