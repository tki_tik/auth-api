package com.portfolio.authapi.security;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

import lombok.RequiredArgsConstructor;

@EnableWebSecurity
@RequiredArgsConstructor
@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    private final SimpleAuthenticationEntryPoint authenticationEntryPoint;

    private final SimpleAccessDeniedHandler accessDeniedHandler;

    private final SimpleAuthenticationSuccessHandler successHandler;

    private final SimpleAuthenticationFailureHandler failureHandler;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
            .authorizeRequests()
            .antMatchers("/webjars/**", "/css/**", "/js/**", "/img/**", "/favicon.ico").permitAll();

        http
            .authorizeRequests()
            .antMatchers("/login", "/api/get-auth-info").permitAll()
            .anyRequest()
            .authenticated()
            .and()

            // 例外
            .exceptionHandling()
            .authenticationEntryPoint(this.authenticationEntryPoint)
            .accessDeniedHandler(this.accessDeniedHandler)
            .and()

            // ログイン設定
            .formLogin()
            .loginProcessingUrl("/login")
            .usernameParameter("userId")
            .passwordParameter("password")
            .successHandler(this.successHandler)
            .failureHandler(this.failureHandler)
            .and()

            // ログアウト設定
            .logout()
            .logoutUrl("/logout")
            .and()

            // Basic認証設定
            .httpBasic()
            .disable()

            // CSRF設定
            .csrf()
            .disable()

            // CORS設定
            .cors();
    }
}