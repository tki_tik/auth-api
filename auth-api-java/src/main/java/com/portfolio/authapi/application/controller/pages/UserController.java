package com.portfolio.authapi.application.controller.pages;

import static com.portfolio.authapi.common.Constant.PAGES_PREFIX;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/pages")
@Controller
public class UserController {

    /**
     * ユーザ画面生成。
     * 
     * @return ユーザ画面。
     */
    @GetMapping("user")
    public String getUser() {
        return PAGES_PREFIX + "/user";
    }
}
