package com.portfolio.authapi.application.controller.api;

import static com.portfolio.authapi.common.Constant.QUERY_KEY_CODE;
import static com.portfolio.authapi.common.Constant.REDIRECT_PREFIX;

import java.net.URI;

import com.portfolio.authapi.domain.service.GetClientHomeUrlService;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.util.UriComponentsBuilder;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@RequestMapping("/api")
@Controller
public class RedirectClientHomeController {

    private final GetClientHomeUrlService service;

    @GetMapping("/redirect-client-home")
    public String redirectClientHome(@RequestParam("clientId") String clientId, @RequestParam("code") String code) {
        String clientHomeUrl = this.service.getClientHomeUrl(clientId);
        return REDIRECT_PREFIX + this.createUrl(clientHomeUrl, code);
    }

    private URI createUrl(String clientHomeUrl, String code) {
        return UriComponentsBuilder.fromHttpUrl(clientHomeUrl).queryParam(QUERY_KEY_CODE, code).build(true).toUri();
    }
}
