package com.portfolio.authapi.application.controller.pages;

import static com.portfolio.authapi.common.Constant.PAGES_PREFIX;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/pages")
@Controller
public class AdminController {

    /**
     * 管理者画面生成。
     * 
     * @return 管理者画面。
     */
    @GetMapping("admin")
    public String getAdmin() {
        return PAGES_PREFIX + "/admin";
    }
}
