package com.portfolio.authapi.application.controller.api;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import com.portfolio.authapi.domain.service.AuthInfoService;
import com.portfolio.authapi.security.dto.AuthInfo;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import lombok.RequiredArgsConstructor;

@RequestMapping("/api")
@RequiredArgsConstructor
@RestController
public class GetAuthInfoController {

    private final AuthInfoService service;

    /**
     * 認証情報取得。
     * 
     * @param response HttpServletResponse
     * @param code 認可コード
     * @return 認証情報
     * @throws IOException
     */
    @GetMapping("/get-auth-info")
    public AuthInfo getAuthInfo(HttpServletResponse response, @RequestParam(name = "code") String code)
        throws IOException {
        AuthInfo authInfo = this.service.getAuthInfo(code);
        if (authInfo.getUserId() == null || authInfo.getUserName() == null || authInfo.getRole() == null) {
            response.sendError(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED.getReasonPhrase());
            return null;
        }
        return authInfo;
    }
}
