package com.portfolio.authapi.common;

public class Constant {

    /**
     * コンストラクタ。
     * インスタンス化させない為private。
     */
    private Constant() {
    }

    /** 権限 */
    public static enum Authority {
        ROLE_USER,
        ROLE_ADMIN
    };

    /** リダイレクト用接頭辞 */
    public static final String REDIRECT_PREFIX = "redirect:";

    /** 画面生成用接頭辞 */
    public static final String PAGES_PREFIX = "/pages";

    /** クエリーキー：認可コード */
    public static final String QUERY_KEY_CODE = "code";

    /** Redis用キー：ユーザID */
    public static final String REDIS_KEY_USER_ID = "user-id:";

    /** Redis用キー：ユーザ名 */
    public static final String REDIS_KEY_USER_NAME = "user-name:";

    /** Redis用キー：権限 */
    public static final String REDIS_KEY_ROLE = "role:";
}
