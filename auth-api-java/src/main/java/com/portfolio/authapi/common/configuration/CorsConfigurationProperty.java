package com.portfolio.authapi.common.configuration;

import java.util.List;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import lombok.Data;
import lombok.NoArgsConstructor;

@Component
@ConfigurationProperties(prefix = "spring.cors")
@Data
@NoArgsConstructor
public class CorsConfigurationProperty {

    /** 許可オリジンリスト */
    List<String> allowedOriginList;
}
